import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import classes from './AuthorControl.module.css';
import * as authorActionTypes from '../../store/actions/authorAction';
import Spinner from '../../components/UI/spinner/Spinner';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft,faArrowRight } from '@fortawesome/free-solid-svg-icons';
import NetworkError from '../../components/UI/networkerror/NetworkError';
import Pagination from 'react-js-pagination'

class AuthorControl extends Component {
    state = {
        searchText:'',
        isvalid:true,
        istouched:false,
        authorsPerPage: 5,
        currentPage:1
    }
    checkvalidity = (value) => {
        let isvalid = true;
        isvalid = value.trim(' ').length >= 3 && isvalid;
        return isvalid;
    }
    handleChange = (event) => {
        const updatedState = {...this.state};
        updatedState.searchText = event.target.value;
        updatedState.isvalid = this.checkvalidity(event.target.value);
        updatedState.isTouched = true;
        this.setState({
            ...updatedState
        })
    }
    handleSubmit = (event) => {
        event.preventDefault();
        if(this.state.isvalid && this.state.isTouched){
            
        } else {
            this.setState({
                error: true
            })
        }
    }
    componentDidMount(){
        this.props.getAuthors();
    }
    handlePageChange = pageNumber => {
        console.log(`active page is ${pageNumber}`);
        this.setState({ currentPage: pageNumber });
      };
    // next = (page) => {
    //     console.log('next');
    //     this.setState({
    //         ...this.state,
    //         currentPage: page+1
    //     })
    // }
    // prev = (page) => {
    //     console.log('next');
    //     this.setState({
    //         ...this.state,
    //         currentPage: page-1
    //     })
    // }
    render() {
        // if(localStorage.getItem('userDetails') === null){
        //     return <Redirect to='/admin/login' />
        // }
        // if(JSON.parse(localStorage.getItem('userDetails')).role !== 'admin'){
        //     return <Redirect to='/admin/login' />
        // }
        if(this.props.fetchErr){
            // console.log('model true')
            return <NetworkError />
        }
        let suggestion = null;
        if(this.props.authorLoading){
            suggestion = <div>
                        <Spinner />
                    </div>
        }
        let refreshedAuthor =   this.props.authors.filter((author)=> {
                                    return author.authorName.toLowerCase().includes(this.state.searchText.toLowerCase());
                                });
        const startValue = (this.state.authorsPerPage * this.state.currentPage) -(this.state.authorsPerPage);
        const endValue = (this.state.authorsPerPage * this.state.currentPage);
        let authorArray = this.props.authors.slice(startValue,endValue);
        const totalPages = Math.ceil(this.props.authors.length/this.state.authorsPerPage);
        console.log(totalPages);
        // let navigate = null;
        // if(totalPages > 1){
        //     if(this.state.currentPage < totalPages){
        //         navigate = <div className={classes.Navigate}>
        //             <p onClick={()=>this.next(this.state.currentPage)} >
        //             next <FontAwesomeIcon icon={faArrowRight} /></p>
        //             <p onClick={()=>this.prev(this.state.currentPage)}>
        //             <FontAwesomeIcon icon={faArrowLeft} /> prev</p>
        //         </div>
        //         if(this.state.currentPage === 1){
        //             navigate = <div className={classes.Navigate}>
        //                 <p onClick={()=>this.next(this.state.currentPage)} >
        //                     next <FontAwesomeIcon icon={faArrowRight} /></p>
        //             </div>
        //         }
        //     }
        //     if(this.state.currentPage >= totalPages){
        //         navigate = <div className={classes.Navigate}>
        //             <p onClick={()=>this.prev(this.state.currentPage)}>
        //             <FontAwesomeIcon icon={faArrowLeft} /> prev</p>
        //         </div>
        //     }
        // }
        suggestion =    <table className={classes.Table}>
                            <tr>
                                <th>S.NO</th>
                                <th>AUTHOR NAME</th>
                            </tr>
                            {
                                authorArray.map( (author,index) => {
                                    return  <tr>
                                                <td>{index+1}</td>
                                                <td key={author.authorId}>
                                                    {author.authorName}
                                                </td>
                                                <td><Link to={'/admin/author/edit/'+author.authorId}>Edit</Link></td>
                                            </tr>
                                })
                            }
                        </table>
        if(this.state.searchText.length > 3 && refreshedAuthor.length > 0){
                suggestion =    <table className={classes.Table}>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>AUTHOR NAME</th>
                                    </tr>
                                    {
                                        refreshedAuthor.map( (author,index) => {
                                            return  <tr>
                                                        <td>{index+1}</td>
                                                        <td key={author.authorId}>
                                                            {author.authorName}
                                                        </td>
                                                        <td><Link to={'/admin/author/edit/'+author.authorId}>Edit</Link></td>
                                                    </tr>
                                        })
                                    }
                                </table>
        }     
        return (
            <div className={classes.Greetings}>
                <h2>Search Author Name</h2>
                 <input 
                    value={this.state.searchText}
                    onChange={this.handleChange} 
                    placeholder='Enter Author Name'
                    />
                <div style={{height:'2rem'}}>
                    <Link to='/admin/author/add' className={classes.Link}>Add New Author</Link>
                </div> 
               <div className={classes.Suggestion}>
                   {suggestion}
               </div>
               {/* {navigate} */}
               <Pagination
                totalItemsCount={totalPages}
                onChange={this.handlePageChange}
                activePage={this.state.currentPage}
                itemsCountPerPage={1}
                pageRangeDisplayed={5}
                innerClass={classes.Pagination}
                // activeClass={classes.Active}
                activeLinkClass={classes.Active}
                disabledClass={classes.Disabled}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        loggedIn : state.loginReducer.loggedIn,
        authors : state.authorReducer.authors,
        authorLoading : state.authorReducer.authorLoading,
        fetchErr : state.authorReducer.fetchErr
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        getAuthors : () => dispatch(authorActionTypes.getAuthors())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AuthorControl);