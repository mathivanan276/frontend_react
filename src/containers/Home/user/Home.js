import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as bookActionType from '../../../store/actions/bookAction';
import Bookcard from '../../../components/bookcard/Bookcard';
import Spinner from '../../../components/UI/spinner/Spinner';
import classes from './Home.module.css';
import SearchBox from '../../../components/searchbox/SearchBox';
import Sidebar from '../../Sidebar/Sidebar';
import Modal from '../../../components/UI/model/Modal';
import NetworkError from '../../../components/UI/networkerror/NetworkError';
import Pagination from 'react-js-pagination'

class Home extends Component {
    state = {
        searchText:'',
        isvalid:true,
        touched:false,
        sidebar:false,
        currentPage:1
    }
    validate = (value) => {
        let isvalid = true;
        isvalid = value.trim(' ').length !== 0 && isvalid;
        return isvalid;
    }
    handleChange = (event) => {
        const updatedState = {...this.state};
        updatedState.searchText = event.target.value;
        updatedState.touched = true;
        updatedState.isvalid = this.validate(event.target.value);
        this.setState({
            ...updatedState
        })
    }
    handleSearch = () => {
        if(this.state.isvalid && this.state.touched){
            this.props.history.push('/search/'+this.state.searchText);
        }
    }
    componentDidMount(){
        this.props.getNewBooks(this.state.currentPage);
    }
    viewBook = (bookId) => {
        this.props.history.push('/view/'+bookId);
    }
    sidebar = () => {
        this.setState({
            ...this.state,
            sidebar: !this.state.sidebar
        })
    }
    handlePageChange = pageNumber => {
        console.log(`active page is ${pageNumber}`);
        this.setState({ currentPage: pageNumber });
        console.log(pageNumber);
        this.props.getNewBooks(pageNumber);
      };
    render() {
        if(this.props.fetchErr){
            // console.log('model true')
            return <NetworkError />
        }
        if(this.props.role === 'admin'){
            return <Redirect to='/admin/home' />
        }
        if(this.props.newBooksLoading){
            return <div><Spinner /></div>
        }
        let list = null;
        if(this.props.newBooks){
            list = this.props.newBooks.map( (book,index) => {
                return <div className={classes.Bookcard}><Bookcard book={book} clicked={()=>this.viewBook(book.bookId)}/></div>
            })
        }
        let modal;
        
        return (
            <div className={classes.Home}>
                <Sidebar />
                <div className={classes.Section}>
                <div className={classes.Main}>
                    <h2>New Arrivals</h2>
                        <SearchBox changed={this.handleChange} isvalid={this.state.isvalid} submit={this.handleSearch} value={this.state.searchText} isValid={this.state.isvalid} />
                        {list}
                    </div>
                </div>
                <Pagination 
                totalItemsCount={this.props.pageCount}
                onChange={this.handlePageChange}
                activePage={this.state.currentPage}
                itemsCountPerPage={1}
                pageRangeDisplayed={2}
                innerClass={classes.Pagination}
                // activeClass={classes.Active}
                activeLinkClass={classes.Active}
                disabledClass={classes.Disabled}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        fetchErr : state.bookReducer.fetchErr,
        role : state.loginReducer.userDetails.role,
        newBooks : state.bookReducer.newBooks,
        pageCount : state.bookReducer.pageCount,
        newBooksLoading : state.bookReducer.newBooksLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getNewBooks : (page) => dispatch(bookActionType.newArrivals(page))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);