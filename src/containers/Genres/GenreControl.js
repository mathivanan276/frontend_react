import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as genreActionTypes from '../../store/actions/genreAction';

import Spinner from '../../components/UI/spinner/Spinner';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import classes from './GenreControl.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft,faArrowRight } from '@fortawesome/free-solid-svg-icons';
import NetworkError from '../../components/UI/networkerror/NetworkError';

class GenreControl extends Component {
    state = {
        searchText:'',
        isvalid:true,
        istouched:false,
        genrePerPage: 5,
        currentPage:1
    }
    checkvalidity = (value) => {
        let isvalid = true;
        isvalid = value.trim(' ').length >= 3 && isvalid;
        return isvalid;
    }
    handleChange = (event) => {
        const updatedState = {...this.state};
        updatedState.searchText = event.target.value;
        updatedState.isvalid = this.checkvalidity(event.target.value);
        updatedState.isTouched = true;
        this.setState({
            ...updatedState
        })
    }
    componentDidMount(){
        this.props.getGenre();
    }
    next = (page) => {
        console.log('next');
        this.setState({
            ...this.state,
            currentPage: page+1
        })
    }
    prev = (page) => {
        console.log('next');
        this.setState({
            ...this.state,
            currentPage: page-1
        })
    }
    render() {
        // if(localStorage.getItem('userDetails') === null){
        //     return <Redirect to='/admin/login' />
        // }
        // if(JSON.parse(localStorage.getItem('userDetails')).role !== 'admin'){
        //     return <Redirect to='/admin/login' />
        // }
        if(this.props.fetchErr){
            // console.log('model true')
            return <NetworkError />
        }
        let suggestion = null;
        if(this.props.genreLoading){
            suggestion = <div>
                        <Spinner />
                    </div>
        }
        let refreshedGenre =   this.props.genre.filter((gerne)=> {
                                    return gerne.genreName.toLowerCase().includes(this.state.searchText.toLowerCase());
                                });
        const startValue = (this.state.genrePerPage * this.state.currentPage) -(this.state.genrePerPage);
        const endValue = (this.state.genrePerPage * this.state.currentPage);
        let genreArray = this.props.genre.slice(startValue,endValue);
        const totalPages = Math.ceil(this.props.genre.length/this.state.genrePerPage);
        let navigate = null;
        if(totalPages > 1){
            if(this.state.currentPage < totalPages){
                navigate = <div className={classes.Navigate}>
                    <p onClick={()=>this.next(this.state.currentPage)} >
                    next <FontAwesomeIcon icon={faArrowRight} /></p>
                    <p onClick={()=>this.prev(this.state.currentPage)}>
                    <FontAwesomeIcon icon={faArrowLeft} /> prev</p>
                </div>
                if(this.state.currentPage === 1){
                    navigate = <div className={classes.Navigate}>
                        <p onClick={()=>this.next(this.state.currentPage)} >
                            next <FontAwesomeIcon icon={faArrowRight} /></p>
                    </div>
                }
            }
            if(this.state.currentPage >= totalPages){
                navigate = <div className={classes.Navigate}>
                    <p onClick={()=>this.prev(this.state.currentPage)}>
                    <FontAwesomeIcon icon={faArrowLeft} /> prev</p>
                </div>
            }
        }
        suggestion =    <table className={classes.Table}>
                            <tr>
                                <th>S.NO</th>
                                <th>AUTHOR NAME</th>
                            </tr>
                            {
                                genreArray.map( (genre,index) => {
                                    return  <tr>
                                                <td>{index+1}</td>
                                                <td key={genre.genreId}>
                                                    {genre.genreName}
                                                </td>
                                                <td><Link to={'/admin/genre/edit/'+genre.genreId}>Edit</Link></td>
                                            </tr>
                                })
                            }
                        </table>
        if(this.state.searchText.length > 3 && refreshedGenre.length > 0){
                suggestion =    <table className={classes.Table}>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>GENRE NAME</th>
                                    </tr>
                                    {
                                        refreshedGenre.map( (genre,index) => {
                                            return  <tr>
                                                        <td>{index+1}</td>
                                                        <td key={genre.genreId}>
                                                            {genre.genreName}
                                                        </td>
                                                        <td><Link to={'/admin/genre/edit/'+genre.genreId}>Edit</Link></td>
                                                    </tr>
                                        })
                                    }
                                </table>
        }
        return (
            <div className={classes.Greetings}>
                <h2>Search Genre Name</h2>
               {/* <SearchBox changed={this.handleChange} submit={this.handleSubmit} value={this.state.searchText} isvalid={this.state.isvalid} /> */}
               <input 
                value={this.state.searchText}
                onChange={this.handleChange} 
                placeholder='Enter Genre Name'
                />
                <div>
                    <Link to='/admin/genre/add' className={classes.Link}>Add New Genre</Link>
                </div> 
               <div className={classes.Suggestion}>
                   {suggestion}
               </div>
               <div>
                   {navigate}
               </div>
            </div>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        loggedIn : state.loginReducer.loggedIn,    
        genre : state.genreReducer.genre,
        genreLoading : state.genreReducer.genreLoading,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        getGenre : () => dispatch(genreActionTypes.getGenre())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(GenreControl);   