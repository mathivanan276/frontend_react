import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as bookActionType from '../../store/actions/bookAction';
import Spinner from '../../components/UI/spinner/Spinner';
import Bookcard from '../../components/bookcard/Bookcard';
import classes from './Search.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faArrowCircleRight, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import Modal from '../../components/UI/model/Modal';

class Search extends Component {
    componentDidMount(){
        this.props.getSearch(this.props.match.params.searchKey);
    }
    render() {
        if(this.props.fetchErr){
            // console.log('model true')
            return <Modal open={true}>
                        <div className={classes.Success}>
                            <h1>Network Error</h1>
                            <div className={classes.Link}><button onClick={()=>window.location.reload(false)}>Click To Refresh</button></div>
                        </div>
                    </Modal>
        }
        if(this.props.booksLoading){
            return <div><Spinner /></div>
        }
        let list = null;
        if(this.props.booksLoading === false){
            if(this.props.books.length > 0){
                list = this.props.books.map( book => {
                    return <Bookcard book={book} clicked={()=>this.props.history.push('/view/'+book.bookId) }/>
                })
            } else {
                list =  <div>
                            <h2>No Books Found</h2>
                            <Link to='/home' ><h4>Go <FontAwesomeIcon icon={faHome} size='2x' /></h4></Link>
                        </div>
            } 
        }
        return (
            <div className={classes.Section}>
                <div onClick={()=> this.props.history.goBack() } style={{cursor:'pointer'}} ><FontAwesomeIcon icon={faArrowCircleLeft} /> Go Back</div>
                <div className={classes.List}>
                    {list}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        books : state.bookReducer.searchedBooks,
        booksLoading : state.bookReducer.searchedBooksLoading,
        fetchErr : state.bookReducer.fetchErr
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getSearch : (searchKey) => dispatch(bookActionType.searchbook(searchKey))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
