import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as genreActionType from '../../store/actions/genreAction';
import classes from './SideBar.module.css';
import { faTimes,faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Modal from '../../components/UI/model/Modal';

class Sidebar extends Component {

    state = {
        open: false
    }

    componentDidMount(){
        this.props.getCategories();
    }
    open = () => {
        this.setState({
            open : !this.state.open
        })
    }
    render() {
        if(this.props.fetchErr){
            // console.log('model true')
            return <Modal open={true}>
                        <div className={classes.Success}>
                            <h1>Network Error</h1>
                            <div className={classes.Link}><button onClick={()=>window.location.reload(false)}>Click To Refresh</button></div>
                        </div>
                    </Modal>
        }
        let SidebarClass = null;
        if(this.state.open){
            SidebarClass = [classes.SidebarMobile,classes.Open];
        } else {
            SidebarClass = [classes.SidebarMobile,classes.Close];
        }
        let categoryList = null;
        if(this.props.categoryLoading === false){
            categoryList =  <ul>
                                {
                                    this.props.category.map( (category,index) => {
                                        if(index < 10){
                                            return <li key={category.genreId} onClick={this.props.close}><Link to={'/search/'+category.genreName} >{category.genreName}</Link></li>
                                        }
                                    })
                                }    
                                <li><Link to='/categories'>more...</Link></li>
                            </ul>
        }
        return (
            <div>
            <div onClick={this.open} className={classes.SidebarArrow}><FontAwesomeIcon icon={ faArrowCircleRight }/> categories</div>
            <div className={SidebarClass.join(' ')}>
                <div className={classes.Section}>
                    <div style={{width:'fit-content',marginLeft:'auto',paddingRight:'0.75rem',cursor:'pointer'}} onClick={this.open}><FontAwesomeIcon icon={faTimes} size='2x' /></div>
                    <h3>Select Category</h3>
                    {categoryList}
                </div>
            </div>
            <div className={classes.Sidebar}>
                <div className={classes.Section}>
                    <h3>Select Category</h3>
                    {categoryList}
                </div>
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        category : state.genreReducer.genre,
        categoryLoading : state.genreReducer.genreLoading,
        fetchErr : state.genreReducer.fetchErr
    }   
}

const mapDispatchToProps = dispatch => {
    return{
        getCategories: () => dispatch(genreActionType.getGenre())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
