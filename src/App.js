import React, { Component, lazy } from "react";
import { connect } from "react-redux";
import * as loginActionType from "./store/actions/loginActions";
import Spinner from "./components/UI/spinner/Spinner";
import Navigation from "./containers/Navigation/Navigation";
import { Route, Switch } from "react-router-dom";
import UserLogin from "./containers/Login/UserLogin/UserLogin";
import UserRegister from "./containers/register/UserRegister";
import Footer from "./components/Footer/Footer";
import AdminLogin from "./containers/Login/AdminLogin/AdminLogin";
import NotFound from "./components/pagenotfound/NotFound";
import { Redirect } from "react-router-dom";
import classes from './App.module.css';
import axios from 'axios'
import SessionExpired from "./components/UI/sessionExpired/SessionExpired";

const AddBook = lazy(() => import("./containers/Books/AddBook/AddBook"));
const EditBook = lazy(() => import("./containers/Books/EditBook/EditBook"));
const AdminHome = lazy(() => import("./containers/Home/admin/AdminHome"));
const AddBookCover = lazy(() =>
  import("./containers/Books/AddBookCover/AddBookCover")
);
const ViewBook = lazy(() => import("./components/adminviewbook/Viewbook"));
const BookControl = lazy(() => import("./containers/Books/BookControl"));
const AuthorControl = lazy(() => import("./containers/Authors/AuthorControl"));
const AddAuthor = lazy(() =>
  import("./containers/Authors/AddAuthor/AddAuthor")
);
const EditAuthor = lazy(() =>
  import("./containers/Authors/EditAuthor/EditAuthor")
);
const PublisherControl = lazy(() =>
  import("./containers/Publisher/PublisherControl")
);
const AddPublisher = lazy(() =>
  import("./containers/Publisher/AddPublisher/AddPublisher")
);
const EditPublisher = lazy(() =>
  import("./containers/Publisher/EditPublisher/EditPublisher")
);
const GenreControl = lazy(() => import("./containers/Genres/GenreControl"));
const AddGenre = lazy(() => import("./containers/Genres/AddGenre/AddGenre"));
const EditGenre = lazy(() => import("./containers/Genres/EditGenre/EditGenre"));
const Stock = lazy(() => import("./containers/Books/AddStock/Stock"));
const Stocks = lazy(() => import("./containers/stocks/Stocks"));
const Cancelled = lazy(() =>
  import("./components/orderspage/cancelled/Cancelled")
);
const Shipped = lazy(() => import("./components/orderspage/shipped/Shipped"));
const Confirmed = lazy(() =>
  import("./components/orderspage/confirmed/Confirmed")
);
const Shipping = lazy(() =>
  import("./components/orderspage/shipping/Shipping")
);

const Home = lazy(() => import("./containers/Home/user/Home"));
const Profile = lazy(() => import("./containers/profile/Profile"));
const EditAddress = lazy(() => import("./containers/address/edit/EditAddress"));
const AddAddress = lazy(() => import("./containers/address/add/AddAddress"));
const Userorders = lazy(() => import("./containers/userorders/Userorders"));
const Cancelorder = lazy(() =>
  import("./containers/userorders/cancelorder/Cancelorder")
);
const Vieworder = lazy(() =>
  import("./containers/userorders/vieworder/Vieworder")
);
const Cart = lazy(() => import("./containers/cart/Cart"));
const Placeorder = lazy(() => import("./containers/buybook/Placeorder"));
const View = lazy(() => import("./containers/viewbook/View"));
const Category = lazy(() => import("./containers/category/Category"));
const Search = lazy(() => import("./containers/search/Search"));

const PrivateRoute = ({component:Component, ...rest}) => {
      return <Route {...rest} render={(props)=>(
        localStorage.getItem('userDetails') ? 
        <Component {...props}/>:
        <Redirect to={{pathname:'/login',state:{from:props.location}}} />
      )} />
      }
const AdminPrivateRoute = ({component:Component, ...rest}) => (
  <Route {...rest} render={(props)=>(
    localStorage.getItem('userDetails') ? 
      (JSON.parse(localStorage.getItem('userDetails')).role === 'admin' ? 
      <Component {...props}/> : 
      <Redirect to={{pathname:'/admin/login',state:{from:props.location}}}/> ):
    <Redirect to='/admin/login' />
  )} />
)

const RestrictedRoute = ({component:Component, ...rest}) => (
  <Route {...rest} render={(props)=>(
    localStorage.getItem('userDetails')? props.history.goBack() :
    <Component {...props} />
  )} />
)

class App extends Component {
  componentDidMount() {
      this.props.checkLogged();
  }
  render() {
    return (
      <>
      <div>
        <Navigation />
        <Switch>
          {/* Admin Routes */}
          <RestrictedRoute path="/login" component={UserLogin} />
          <RestrictedRoute path="/admin/login" component={AdminLogin} />
          <RestrictedRoute path="/register" component={UserRegister} />
          <React.Suspense fallback={<Spinner />}>
            <Switch>
              <AdminPrivateRoute path="/admin/stocks" component={Stocks} />
              <AdminPrivateRoute path="/admin/genre/add" component={AddGenre} />
              <AdminPrivateRoute path="/admin/genre/edit/:genreId" component={EditGenre} />
              <AdminPrivateRoute path="/admin/genre" exact component={GenreControl} />
              <AdminPrivateRoute path="/admin/publisher/add" component={AddPublisher} />
              <AdminPrivateRoute
                path="/admin/publisher/edit/:publisherId"
                component={EditPublisher}
              />
              <AdminPrivateRoute
                path="/admin/publisher"
                exact
                component={PublisherControl}
              />
              <AdminPrivateRoute path="/admin/author/add" component={AddAuthor} />
              <AdminPrivateRoute
                path="/admin/author/edit/:authorId"
                component={EditAuthor}
              />
              <AdminPrivateRoute path="/admin/author" exact component={AuthorControl} />
              <AdminPrivateRoute
                path="/admin/book/cover/:bookId"
                component={AddBookCover}
              />
              <AdminPrivateRoute
                path="/admin/book/stock/:bookId/:quantity"
                component={Stock}
              />
              <AdminPrivateRoute path="/admin/book/edit/:bookId" component={EditBook} />
              <AdminPrivateRoute path="/admin/book/view/:bookId" component={ViewBook} />
              <AdminPrivateRoute path="/admin/book/add" component={AddBook} />
              <AdminPrivateRoute path="/admin/book" exact component={BookControl} />
              <AdminPrivateRoute
                path="/admin/orders/confirmed/:cartId/:orderId"
                component={Confirmed}
              />
              <AdminPrivateRoute
                path="/admin/orders/shipping/:cartId/:orderId"
                component={Shipping}
              />
              <AdminPrivateRoute
                path="/admin/orders/shipped/:cartId/:orderId"
                component={Shipped}
              />
              <AdminPrivateRoute
                path="/admin/orders/cancelled/:cartId/:orderId"
                component={Cancelled}
              />
              <AdminPrivateRoute path="/admin/home" exact component={AdminHome} />
              {/* User Routes */}
              <Route path="/categories" component={Category} />
              <PrivateRoute path="/address/edit/:addressId" islogged={this.props.loggedIn} component={EditAddress} />
              <PrivateRoute path="/address/add" islogged={this.props.loggedIn} component={AddAddress} />
              <PrivateRoute path="/profile" islogged={this.props.loggedIn} component={Profile} />
              <PrivateRoute
                path="/orders/cancelorder/:cartId"
                component={Cancelorder}
                islogged={this.props.loggedIn}
              />
              <PrivateRoute path="/orders/vieworder/:cartId" islogged={this.props.loggedIn} component={Vieworder} />
              <PrivateRoute path="/orders" islogged={this.props.loggedIn} exact component={Userorders} />
              <PrivateRoute path="/buy" islogged={this.props.loggedIn} exact component={Placeorder} />
              <PrivateRoute path="/cart" islogged={this.props.loggedIn} exact component={Cart} />
              <Route path="/view/:bookId" component={View} />
              <Route path="/search/:searchKey" component={Search} />
              <Route path="/home" exact component={Home} />
              <Route path="/" exact component={Home} />
              <Route path="/" component={()=> <NotFound />} />
            </Switch>
          </React.Suspense>
        </Switch>
        <Footer />
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading : state.loginReducer.isLoading,
    loggedIn : state.loginReducer.loggedIn,
    expires : state.loginReducer.userDetails.expiresIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkLogged: () => dispatch(loginActionType.checkLoggedIn()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
