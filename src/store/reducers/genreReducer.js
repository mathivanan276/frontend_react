import * as actionType from '../actions/genreAction'; 

const initialState = {
    genre: [],
    genreLoading : true,
    fetchErr: false
}

const reducer = (state = initialState , action) => {

    switch(action.type){
        case actionType.GET_GENRE :
            return {
                ...state,
                genre: action.data.data
            }
        case actionType.GENRE_LODING_TRUE :
            return {
                ...state,
                genreLoading: true
            }
        case actionType.GENRE_LODING_FALSE :
            return {
                ...state,
                genreLoading: false
            }
        case actionType.FETCH_ERR_GENRE:
            return {
                ...state,
                fetchErr:true
            }
        default :
            return state;
    }
}

export default reducer;