import * as orderActionType from '../actions/orderSummaryAction';

const initialState = {
    orderSummary : {},
    orderSummaryLoading: true,
    fetchErr:false
}

const reducer = (state = initialState , action ) => {
    switch(action.type){
        case (orderActionType.GET_ORDER_SUMMARY) :
            return {
                ...state,
                orderSummary: action.data
            }
        case (orderActionType.ORDERS_SUMMARY_LOADING_TRUE):
                return {
                    ...state,
                    orderSummaryLoading:true
                }
        case (orderActionType.ORDERS_SUMMARY_LOADING_FALSE):
                return {
                    ...state,
                    orderSummaryLoading:false
                }
        case orderActionType.FETCH_ERR_ORDER_SUMMARY:
            return {
                ...state,
                fetchErr:true
            }
        default :
            return false;
    }
}

export default reducer;