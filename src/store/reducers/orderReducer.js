import * as orderActionType from '../actions/orderAction';

const initialState = {
    orders : {},
    ordersLoading: true,
    fetchErr : false
}

const reducer = (state = initialState , action ) => {
    switch(action.type){
        case (orderActionType.GET_ORDERS) : 
            return {
                ...state,
                orders: action.data
            }

        case (orderActionType.ORDERS_LOADING_TRUE):
                return {
                    ...state,
                    ordersLoading:true
                }
        case (orderActionType.ORDERS_LOADING_FALSE):
                return {
                    ...state,
                    ordersLoading:false
                }
        case orderActionType.FETCH_ERR_ORDER: 
            return {
                ...state,
                fetchErr:false
            }
        default :
            return false;
    }
}

export default reducer;