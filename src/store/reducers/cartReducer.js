import * as actionTypes from '../actions/cartAction';

const initialState = {
    cart : {},
    cartLoading: true,
    fetchErr:false,
    empty:false
}

const reducer = (state=initialState , action ) => {
    switch(action.type) {
        case actionTypes.CART_LOADING_TRUE : {
            return {
                ...state,
                cartLoading:true
            }
        }
        case actionTypes.CART_LOADING_FALSE : {
            return {
                ...state,
                cartLoading:false
            }
        }
        case actionTypes.GET_CART : {
            return {
                ...state,
                cart: action.data
            }
        }
        case actionTypes.FETCH_ERR_CART: {
            return {
                ...state,
                fetchErr:true
            }
        }
        case actionTypes.EMPTY_CART: {
            return {
                ...state,
                empty:action.data
            }
        }
        default : 
            return state;
    }
}

export default reducer;