import Axios from 'axios';
import {logout} from './loginActions'

export const CART_LOADING_TRUE = 'CART_LOADING_TRUE';
export const CART_LOADING_FALSE = 'CART_LOADING_FALSE';
export const GET_CART = 'GET_CART';
export const FETCH_ERR_CART = 'FETCH_ERR_CART';
export const EMPTY_CART = 'EMPTY_CART';

const cartLoadingTrue = () => {
    return {
        type:CART_LOADING_TRUE
    }
}
const cartLoadingFalse = () => {
    return {
        type:CART_LOADING_FALSE
    }
}

const saveCart = (data) => {
    return{
        type:GET_CART,
        data
    }
}

const fetchErr = (data) => {
    console.log('fetch err');
    return {
        type: FETCH_ERR_CART,
        data
    }
}

export const cartEmpty = (data) => {
    return {
        type: EMPTY_CART,
        data
    }
}

export const getCart = (userId) => {
    return (dispatch) => {
        dispatch(cartLoadingTrue());
        if(localStorage.getItem('userDetails')){
            Axios.get('carts/read')
            .then (res => {
                console.log(res.data)
                if(res.data.response === true){
                    dispatch(saveCart(res.data.data));
                    dispatch(cartLoadingFalse());
                }
                 else {
                    //  dispatch(logout())
                    dispatch(saveCart(res.data.data));
                    dispatch(cartLoadingFalse());
                }
            })
            .catch(err => {
                console.log(err);
                dispatch(fetchErr(err));
            })
        }
    }
}