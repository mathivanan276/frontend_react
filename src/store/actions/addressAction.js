import Axios from 'axios';
import { checkLoggedIn,logout } from './loginActions';

export const ADDRESS_LOADING_TRUE = 'ADDRESS_LOADING_TRUE';
export const ADDRESS_LOADING_FALSE = 'ADDRESS_LOADING_FALSE';
export const GET_ADDRESS = 'GET_ADDRESS';
export const FETCH_ERR_ADDRESS = 'FETCH_ERR_ADDRESS';
export const SESSION_EXPIRED = 'SESSION_EXPIRED';


const addressLoadingTrue = () => {
    return{
        type:ADDRESS_LOADING_TRUE
    }
}

const addressLoadingFalse = () => {
    return{
        type:ADDRESS_LOADING_FALSE
    }
}

const saveAddress = (data) => {
    return {
        type:GET_ADDRESS,
        data:data
    }
}

const fetchErr = (data) => {
    console.log('fetch err');
    return {
        type: FETCH_ERR_ADDRESS,
        data
    }
}

const sessionExpired = () => {
    return {
        type:SESSION_EXPIRED
    }
}

export const getAddress = () => {
    return (dispatch) => {
        dispatch(addressLoadingTrue());
        if(localStorage.getItem('userDetails')){
        const token = JSON.parse(localStorage.getItem('userDetails')).token;
        Axios.get('address/read',{
            headers:{'HTTP_AUTHORIZATION' : token}
        })
        .then(res => {
            if(res.data.response === true) {
                dispatch(saveAddress(res.data.data))
                dispatch(addressLoadingFalse());
            } else {
                dispatch(logout());
                // localStorage.setItem('expired','true')
            }
        })
        .catch(err => {
            dispatch(fetchErr(err));
        })
    }
}
}