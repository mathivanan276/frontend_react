import Axios from "axios";
import * as loginActionTypes from './loginActions';

export const GET_ORDERS = "SET_ORDERS";
export const ORDERS_LOADING_TRUE = "ORDERS_LOADING_TRUE";
export const ORDERS_LOADING_FALSE = "ORDERS_LOADING_FALSE";
export const FETCH_ERR_ORDER = 'FETCH_ERR_ORDER';

const saveOrders = (data) => {
  return {
    type: GET_ORDERS,
    data: data,
  };
};

const ordersLoadingTrue = () => {
  return {
    type: ORDERS_LOADING_TRUE,
  };
};

const ordersLoadingFalse = () => {
  return {
    type: ORDERS_LOADING_FALSE,
  };
};

export const sortedOrders = (sortDate) => {
  return (dispatch) => {
    dispatch(ordersLoadingTrue());
    if (localStorage.getItem("userDetails")) {
      // let token = JSON.parse(localStorage.getItem("userDetails")).token;
      Axios.post(
        "/orders/specificdate",
        { date: sortDate },
        {
          // headers: { HTTP_AUTHORIZATION: token },
        }
      ).then((res) => {
        console.log(res);
        if (res.data.response === true) {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
        }
         else {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
          dispatch(loginActionTypes.logout());
        }
      }).catch(err => {
        dispatch(fetchErr(err))
      })
    }
  };
};
export const rangedOrders = (date1, date2) => {
  return (dispatch) => {
    dispatch(ordersLoadingTrue());
    if (localStorage.getItem("userDetails")) {
      Axios.post(
        "/orders/range",
        { date1, date2 }
      ).then((res) => {
        console.log(res);
        if (res.data.response === true) {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
        }
         else {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
          dispatch(loginActionTypes.logout());
        }
      }).catch(err => {
        dispatch(fetchErr(err));
      })
    }
  };
};
export const getorders = () => {
  return (dispatch) => {
    dispatch(ordersLoadingTrue());
    if (localStorage.getItem("userDetails")) {
      Axios.get("/orders/lastsevendays").then((res) => {
        // console.log(res);
        if (res.data.response === true) {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
        }
         else {
          dispatch(saveOrders(res.data.data));
          dispatch(ordersLoadingFalse());
          dispatch(loginActionTypes.logout());
        }
      }).catch(err => {
        dispatch(fetchErr(err));
      })
    }
  };
};
const fetchErr = (data) => {
  console.log('fetch err');
  return {
      type: FETCH_ERR_ORDER,
      data
  }
}