import axios from 'axios';
import { checkLoggedIn,logout } from './loginActions';

export const GET_AUTHORS = 'GET_AUTHORS';
export const AUTHOR_LODING_TRUE = 'AUTHOR_LODING_TRUE';
export const AUTHOR_LODING_FALSE = 'AUTHOR_LODING_FALSE';
export const FETCH_ERR_AUTHOR = 'FETCH_ERR_AUTHOR';

const storeAuthors = (data) => {
    // console.log(data);
    return {
        type:GET_AUTHORS,
        data:data
    }
}

const authorLoadingTrue = () => {
    return {
        type:AUTHOR_LODING_TRUE
    }
}

const authorLoadingFalse = () => {
    return {
        type:AUTHOR_LODING_FALSE
    }
}

const fetchErr = (data) => {
    console.log('fetch err');
    return {
        type: FETCH_ERR_AUTHOR,
        data
    }
}

export const getAuthors = () => {
    return (dispatch) => {
        dispatch(authorLoadingTrue());
        axios.get('authors/read')
        .then(res => {
            // console.log(res.data);
            if(res.data.response === true){
                dispatch(storeAuthors(res.data));
                dispatch(authorLoadingFalse());
            } else {
                dispatch(logout());
            }
        })
        .catch(err => {
            console.log(err);
            dispatch(fetchErr(err))
        })
    }
}