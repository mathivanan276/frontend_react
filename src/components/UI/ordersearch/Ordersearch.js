import React,{useState} from 'react';
import classes from './Ordersearch.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilter } from '@fortawesome/free-solid-svg-icons'


export default function Ordersearch(props) {
    let baseYear = 1998;
    let currentYear = new Date().getFullYear();
    let currentMonth = new Date().getUTCMonth();
    const [sortBy,setSortBy] = useState('date');
    const [month, setMonth] = useState(currentMonth+1);
    const [year,setYear] = useState(currentYear);
    const [date1,setDate1] = useState('');
    const [date2,setDate2] = useState('');
    const [error,setError] = useState(false);
    const [openFilter,setopenFilter] = useState(false);
    let sort =  <div className={classes.Type1} >
                    <input type='date' onChange={(event)=>props.getsearch(event.target.value)}/>
                </div>
    if(sortBy === 'month'){
        let year = new Array();
        for(let i=currentYear ; i>=baseYear ; i--){
            year.push(i);
        }
        sort =  <div className={classes.Type2}>
                    <div>
                    <select onChange={(event)=>{
                                setMonth(event.target.value)
                            }} defaultValue={month}>
                                <option value='01'>Jan</option>
                                <option value='02'>Feb</option>
                                <option value='03'>Mar</option>
                                <option value='04'>Apr</option>
                                <option value='05'>May</option>
                                <option value='06'>Jun</option>
                                <option value='07'>July</option>
                                <option value='08'>Aug</option>
                                <option value='09'>Sep</option>
                                <option value='10'>Oct</option>
                                <option value='11'>Nov</option>
                                <option value='12'>Dec</option>
                            </select>
                    </div>
                    <div>
                    <select onChange={(event)=>{
                                setYear(event.target.value)
                            }}>
                                {
                                    year.map(year=>{
                                        return <option value={year} key={year+1}>{year}</option>
                                    })
                                }
                            </select>
                    </div>
                    <div>
                        <span
                            onClick={()=>props.getsearch(year+'-'+month)}
                            style={{marginLeft:'0.75rem',color:'#3170b2', lineHeight:'2rem'}}>search</span>
                    </div>
                </div>
    }
    if(sortBy === 'range'){
        sort =  <div className={classes.Type3}>
                    <input type='date' onChange={(event)=>setDate1(event.target.value)} className={error ? classes.Error : null} />
                    <span style={{lineHeight:'2rem'}}> to </span>
                    <input type='date' onChange={(event)=>setDate2(event.target.value)} className={error ? classes.Error : null} />
                    <p onClick={()=>{
                        if(date1 !== '' && date2 !== '' && date2 > date1 && date1 !== date2 && new Date(date2) < new Date()){
                            props.getRange(date1,date2)
                            setError(false)
                        } else {
                            setError(true)
                        }
                    }}
                    style={{marginLeft:'0.75rem',color:'#3170b2',lineHeight:'2rem'}}>search</p>
                </div>
    }
    let openclass = [classes.MobileFilter,classes.Close];
    if(openFilter){
        openclass = [classes.MobileFilter,classes.Open];
    }
    return (
        <div className={classes.Section}>   
            <div className={classes.Mobile}>
                <div style={{textAlign:'right', padding:'0.75rem',color:'#f4f4f4'}}><span onClick={()=>setopenFilter(!openFilter)}><FontAwesomeIcon icon={faFilter} size='1x'  /> Filter</span></div>
                <div className={openclass.join(' ')}>
                    <div className={classes.MobileType}>
                        <select onChange={(event)=>{setSortBy(event.target.value)}}>
                            <option value='date'>Date</option>
                            <option value='month'>Month/Year</option>
                            <option value='range'>Range</option>
                        </select>
                    </div>
                    {sort}
                </div>
            </div>
            <div className={classes.Desktop}>
                <div className={classes.DesktopFilter}>
                    <select onChange={(event)=>{setSortBy(event.target.value)}}>
                        <option value='date'>Date</option>
                        <option value='month'>Month/Year</option>
                        <option value='range'>Range</option>
                    </select>
                    {sort}
                </div>
            </div>
        </div>
    )
}
