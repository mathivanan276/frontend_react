import React from 'react';
import  classes from './NetworkError.module.css';
import Modal from '../model/Modal';
export default function NetworkError() {
    return (
        <Modal open={true}>
            <div className={classes.Error}>
                <h1>Network Error</h1>
                <div className={classes.Link}><button onClick={()=>window.location.reload(false)}>Click To Refresh</button></div>
            </div>
        </Modal>
    )
}