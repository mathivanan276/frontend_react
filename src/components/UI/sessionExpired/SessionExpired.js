import React from 'react';
import  classes from './SessionExpired.module.css';
import Modal from '../model/Modal';
import {Link } from 'react-router-dom';
import { useHistory } from 'react-router';
export default function SessionExpired(props) {
    const history = useHistory();
    let handleClick = () => {
        localStorage.removeItem('session');
        history.push('/login');
    }
    const open = localStorage.getItem('session')|| false
    return (
        <Modal open={open}>
            <div className={classes.Error}>
                <h1>Session Expired</h1>
                <div className={classes.Link}><h4 onClick={handleClick}>click to Login</h4></div>
            </div>
        </Modal>
    )
}
