// let set = Object.create(null);

// set.foo = 'true';
// set.foo = 'false';

// // checking for existence
// if (set.foo) {
//     console.log(set.foo)
//     // do something
// }
// let set = new Set([1, 2]);

// set.forEach(function(value, key, ownerSet) {
//     console.log(key + " " + value);
//     console.log(ownerSet === set);
// });
let set = new Set(),
    key = {'name':'mathi'};

set.add(key);
console.log(set.size);      // 1

// eliminate original reference
key = null;

console.log(set.size);      // 1

// get the original reference back
key = [...set][0];
console.log(['a','hi'])